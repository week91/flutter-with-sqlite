class Cat {
  final int id;
  final String name;
  final int age;

  Cat({required this.id, required this.name, required this.age});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  static List<Cat> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Cat(
        id: maps[i]['id'],
        name: maps[i]['name'],
        age: maps[i]['age'],
      );
    });
  }

  @override
  String toString() {
    return 'Cat {id: $id, name: $name, age: $age}';
  }
}